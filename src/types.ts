export type Currency = 'USD' | 'EUR' | 'GBP';

export type CurrencyRate = Record<Currency, number>;

export type Account = {
  id: number;
  currency: Currency;
  amount: number;
};

export type ExchangeType = 'sell' | 'buy';

export type TransferType = 'from' | 'to';

export type TransactionItem = {
  currency: Currency;
  amount: number;
};

export type Transaction = {
  id: string;
  items: Array<TransactionItem>;
};
