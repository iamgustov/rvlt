const abortableFetch = (request: string, opts: Record<string, any> = {}) => {
  let controller = new AbortController();

  return {
    abort: () => {
      controller.abort();
      controller = new AbortController();
    },
    go: () => fetch(request, { ...opts, signal: controller.signal }),
  };
};

export default abortableFetch;
