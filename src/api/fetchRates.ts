import { CurrencyRate, Currency } from 'types';
import abortableFetch from './abortableFetch';

const API_URL = '/stats/eurofxref/eurofxref-daily.xml';
const request = abortableFetch(API_URL);

const parseRates = (xml: string): CurrencyRate => {
  const parse = new window.DOMParser().parseFromString(xml, 'text/xml');
  const nodes = parse.querySelectorAll('[currency]');
  return Array.from(nodes).reduce((acct, node) => {
    const code = node.getAttribute('currency') as Currency;
    const rate = parseFloat(node.getAttribute('rate') ?? '');
    return {
      ...acct,
      [code]: rate,
    };
  }, {} as CurrencyRate);
};

const fetchRates = async (): Promise<CurrencyRate> => {
  request.abort();
  const response = await request.go();
  const xml = await response.text();
  return parseRates(xml);
};

export default fetchRates;
