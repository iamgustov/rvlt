import * as calculator from 'utils/calculator';
import { round } from 'utils/misc';
import { Account, CurrencyRate, ExchangeType, Transaction } from 'types';

const ACCOUNTS: Array<Account> = [
  { id: 1, currency: 'EUR', amount: 339642 },
  { id: 2, currency: 'USD', amount: 13 },
  { id: 3, currency: 'GBP', amount: 50000 },
];

const createStore = () => {
  const store = {
    accounts: ACCOUNTS,
    accountFromId: ACCOUNTS[0].id,
    accountToId: ACCOUNTS[1].id,
    type: 'sell' as ExchangeType,
    rates: undefined as CurrencyRate | undefined,
    amountFrom: undefined as number | undefined,
    amountTo: undefined as number | undefined,
    transactions: [] as Array<Transaction>,
    get from() {
      return this.accounts.find(
        (account) => account.id === this.accountFromId,
      )!;
    },
    set from(account: Account) {
      this.accountFromId = account.id;
    },
    get to() {
      return this.accounts.find((account) => account.id === this.accountToId)!;
    },
    set to(account: Account) {
      this.accountToId = account.id;
    },
    get rate() {
      if (!this.rates) return 1;
      return calculator.rateOf({
        rates: this.rates,
        from: this.from.currency,
        to: this.to.currency,
      });
    },
    get pendingTransaction(): Transaction | undefined {
      const [from, to] = this.isSellMode
        ? [this.from, this.to]
        : [this.to, this.from];
      const amount = this.isSellMode ? this.amountFrom : this.amountTo;
      if (!amount || !this.rates) return undefined;

      return calculator.exchange({
        from: from.currency,
        to: to.currency,
        amount,
        rates: this.rates,
      });
    },
    get isFromExceeds() {
      if (!this.pendingTransaction) return false;
      const fromTransaction = this.pendingTransaction.items.find(
        (item) => item.currency === this.from.currency,
      );
      if (!fromTransaction) return false;
      return this.from.amount + fromTransaction.amount <= 0;
    },
    get isToExceeds() {
      if (!this.pendingTransaction) return false;
      const toTransaction = this.pendingTransaction.items.find(
        (item) => item.currency === this.to.currency,
      );
      if (!toTransaction) return false;
      return this.to.amount + toTransaction.amount <= 0;
    },
    get isValid() {
      // if rates not yet loaded
      if (!this.rates) return false;
      // if exceeds
      if (this.isToExceeds || this.isFromExceeds) return false;
      // if it's not same currency & amounts > 0
      return this.from.id !== this.to.id && this.amountFrom && this.amountTo;
    },
    get isSellMode() {
      return this.type === 'sell';
    },
    get lastTransaction() {
      return this.transactions[this.transactions.length - 1];
    },
    toggleType() {
      this.type = this.isSellMode ? 'buy' : 'sell';
    },
    setRates(rates: CurrencyRate) {
      this.rates = rates;
    },
    changeFromAccount(account: Account) {
      this.from = account;
      if (this.amountFrom) {
        this.changeAmountFrom(this.amountFrom);
      }
    },
    changeToAccount(account: Account) {
      this.to = account;
      if (this.amountTo) {
        this.changeAmountTo(this.amountTo);
      }
    },
    changeAmountFrom(amount: number) {
      this.amountFrom = amount;
      this.amountTo = round(amount * this.rate);
    },
    changeAmountTo(amount: number) {
      this.amountTo = amount;
      this.amountFrom = round(amount / this.rate);
    },
    exchange() {
      if (!this.isValid) return;
      if (!this.pendingTransaction) return;

      this.accounts.forEach((account) => {
        const item = this.pendingTransaction!.items.find(
          (t) => t.currency === account.currency,
        );
        if (item) {
          account.amount += item.amount;
        }
      });

      this.transactions.push(this.pendingTransaction);
      this.amountFrom = undefined;
      this.amountTo = undefined;
    },
  };

  return store;
};

export type Store = ReturnType<typeof createStore>;
export default createStore;
