import React, { ReactNode, createContext, useContext } from 'react';
import { useLocalStore } from 'mobx-react-lite';
import createStore, { Store } from './store';

const Context = createContext<Store | null>(null);

export const Provider = ({
  children,
}: {
  children: ReactNode;
}): JSX.Element => {
  const store = useLocalStore(createStore);
  return <Context.Provider value={store}>{children}</Context.Provider>;
};

export const useStore = () => {
  const store = useContext(Context);
  if (!store) {
    throw new Error('useStore must be used within a Provider.');
  }
  return store;
};
