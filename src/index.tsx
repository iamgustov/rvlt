import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import Theme from 'theme';
import { Provider } from 'store';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <StrictMode>
    <Provider>
      <Theme>
        <App />
      </Theme>
    </Provider>
  </StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
