import React, { ReactNode } from 'react';
import { ThemeProvider as EmotionhemeProvider } from '@emotion/react';
import theme from './theme';

interface ThemeProviderProps {
  children: ReactNode;
}

const ThemeProvider = ({ children }: ThemeProviderProps): JSX.Element => (
  <EmotionhemeProvider theme={theme}>{children}</EmotionhemeProvider>
);

export default ThemeProvider;
