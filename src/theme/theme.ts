const SPACING_LIMIT = 96;

const colors = {
  bg: 'rgb(243, 244, 245)',
  card: 'white',
  text: 'rgb(25, 28, 31)',
  white: 'white',
  gray: 'rgb(117, 128, 138)',
  brand: 'rgb(6, 102, 235)',
  outline: '#d9e8f7',
  error: '#ff0033',
  overlay: 'rgba(0, 0, 0, 0.5)',
};

const space = [...new Array(SPACING_LIMIT)].reduce(
  (acc, cur, x) => ({
    ...acc,
    [x]: x * 4,
    [x / 2]: x * 2,
    [`-${x}`]: -(x * 4),
    [`-${x / 2}`]: -(x * 4),
  }),
  {},
);

const fontSizes = [10, 12, 14, 16, 20, 24, 32];

const theme = {
  colors,
  space,
  fontSizes,
};

export default theme;
