import React from 'react';
import { Global, css } from '@emotion/react';

const Styles = () => (
  <Global
    styles={css`
      body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
          'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
          'Helvetica Neue', sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        background: rgb(243, 244, 245);
        color: rgb(25, 28, 31);
      }

      html,
      body {
        height: 100%;
      }

      * {
        box-sizing: border-box;
      }

      #root {
        min-height: 100%;
      }
    `}
  />
);

export default Styles;
