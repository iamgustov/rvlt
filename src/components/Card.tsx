import React, { ReactNode } from 'react';
import Box from 'components/Box';

interface CardProps {
  children: ReactNode;
}

const Card = ({ children }: CardProps): JSX.Element => (
  <Box borderRadius={10} px={4} py={3} bg="card">
    {children}
  </Box>
);

export default Card;
