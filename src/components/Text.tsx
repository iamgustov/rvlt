import styled from '@emotion/styled';
import { typography, color } from 'styled-system';

const Text = styled('div')(typography, color);

export default Text;
