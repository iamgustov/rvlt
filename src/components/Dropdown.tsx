import React, { ChangeEvent, useMemo } from 'react';
import styled from '@emotion/styled';
import { ChevronDown } from '@emotion-icons/fluentui-system-regular';
import Box from 'components/Box';
import { uniqId } from 'utils/misc';

interface DropdownOption {
  label: string;
  value: number;
}

interface DropdownProps {
  value: number;
  options: Array<DropdownOption>;
  onChange: (value: number) => void;
}

const Select = styled.select`
  appearance: none;
  outline: none;
  font-size: 18px;
  font-weight: 600;
  border: none;
  padding-right: 20px;
  cursor: pointer;
`;

const Dropdown = ({ value, options, onChange }: DropdownProps): JSX.Element => {
  const name = useMemo(uniqId, []);
  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    onChange(Number(event.target.value));
  };
  return (
    <Box
      position="relative"
      cursor="pointer"
      display="inline-flex"
      alignItems="center"
    >
      <Select value={value} name={name} onChange={handleChange}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </Select>
      <Box ml={0.5} position="absolute" right={0} top={0}>
        <ChevronDown size={18} />
      </Box>
    </Box>
  );
};

export default Dropdown;
