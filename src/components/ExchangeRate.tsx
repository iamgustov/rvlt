import React from 'react';
import { ArrowTrending } from '@emotion-icons/fluentui-system-regular';
import Text from 'components/Text';
import Box from 'components/Box';
import { useTheme } from '@emotion/react';
import { currency as displayCurrency } from 'utils/display';
import { Currency } from 'types';

interface ExchangeRateProps {
  rate: number;
  from: Currency;
  to: Currency;
}

const ExchangeRate = ({ rate, from, to }: ExchangeRateProps): JSX.Element => {
  const baseRate = displayCurrency(1, from);
  const convertRate = displayCurrency(rate, to);
  // @ts-ignore
  const iconColor = useTheme().colors.brand;
  return (
    <Box display="flex" alignItems="center">
      <ArrowTrending size={16} color={iconColor} />
      <Box ml={1} mt={-0.5}>
        <Text color="brand" fontSize={2} fontWeight="600">
          {baseRate} = {convertRate}
        </Text>
      </Box>
    </Box>
  );
};

export default ExchangeRate;
