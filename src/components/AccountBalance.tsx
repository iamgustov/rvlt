import React from 'react';
import Dropdown from 'components/Dropdown';
import Card from 'components/Card';
import Box from 'components/Box';
import Text from 'components/Text';
import Input from 'components/Input';
import { currency as displayCurrency } from 'utils/display';
import { Account, ExchangeType } from 'types';

interface AccountBalanceProps {
  account: Account;
  accounts: Array<Account>;
  amount?: number;
  exceeds?: boolean;
  type: ExchangeType;
  onAmountChange: (number: number) => void;
  onAccountChange: (account: Account) => void;
}

const AccountBalance = ({
  account,
  accounts,
  amount,
  exceeds = false,
  type = 'sell',
  onAmountChange,
  onAccountChange,
}: AccountBalanceProps): JSX.Element => {
  const balance = displayCurrency(account.amount, account.currency);
  const accountOptions = accounts.map((account) => ({
    value: account.id,
    label: account.currency,
  }));
  const handleAmountChange = (amount: string) => {
    onAmountChange(Number(amount));
  };
  const handleAccountChange = (value: number) => {
    const newAccount = accounts.find((account) => account.id === value);
    if (newAccount) {
      onAccountChange(newAccount);
    }
  };
  return (
    <Card>
      <Box display="flex" alignItems="flex-start">
        <Box flex={1}>
          <Dropdown
            value={account.id}
            options={accountOptions}
            onChange={handleAccountChange}
          />
          <Text fontSize={2} color="gray">
            Balance: {balance}
          </Text>
        </Box>
        <Box width="30%">
          <Input
            prefix={type === 'sell' ? '-' : '+'}
            value={amount ? amount : ''}
            placeholder="0"
            onChange={handleAmountChange}
          />
          {exceeds && (
            <Box mt={-1}>
              <Text fontSize={1} color="error" textAlign="right">
                exceeds balance
              </Text>
            </Box>
          )}
        </Box>
      </Box>
    </Card>
  );
};

export default AccountBalance;
