import React, { ReactNode } from 'react';
import Box from 'components/Box';

interface ContainerProps {
  children: ReactNode;
}

const Container = ({ children }: ContainerProps): JSX.Element => (
  <Box
    bg="bg"
    minHeight="100vh"
    py={[4, 0]}
    px={[4, 6]}
    display={['block', 'flex', 'flex']}
    alignItems="center"
    justifyContent="center"
  >
    <Box width={['100%', '80%', '40%']}>{children}</Box>
  </Box>
);

export default Container;
