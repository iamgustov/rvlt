import React from 'react';
import Box from 'components/Box';
import { SpinnerIos } from '@emotion-icons/fluentui-system-regular';
import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';

interface LoaderProps {
  size?: number;
  fullSize?: boolean;
}

const LoaderIcon = styled(SpinnerIos)`
  animation: spin 1s ease-in-out infinite;

  @keyframes spin {
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
`;

const Loader = ({ size = 30, fullSize = false }: LoaderProps): JSX.Element => {
  // @ts-ignore
  const iconColor = useTheme().colors.brand;
  const loader = <LoaderIcon size={size} color={iconColor} />;
  if (!fullSize) return loader;
  return (
    <Box
      position="fixed"
      display="flex"
      alignItems="center"
      justifyContent="center"
      top={0}
      left={0}
      right={0}
      height="100vh"
    >
      {loader}
    </Box>
  );
};

export default Loader;
