import React from 'react';
import styled from '@emotion/styled';
import { typography, space, color, size, layout, border } from 'styled-system';

interface ButtonProps {
  variant?: 'primary' | 'outline';
  children: string | Array<string>;
  onClick?: () => void;
  disabled?: boolean;
}

const StyledButton = styled('button')(
  {
    cursor: 'pointer',
    '&:disabled': {
      opacity: 0.5,
    },
  },
  typography,
  space,
  color,
  size,
  layout,
  border,
);

const Button = ({
  variant = 'primary',
  children,
  disabled = false,
  onClick,
}: ButtonProps): JSX.Element => (
  <StyledButton
    bg={variant === 'primary' ? 'brand' : 'outline'}
    color={variant === 'primary' ? 'white' : 'brand'}
    borderRadius={10}
    p={2.5}
    fontSize={2}
    fontWeight="600"
    display="block"
    width="100%"
    appearance="none"
    border="none"
    cursor="pointer"
    disabled={disabled}
    onClick={onClick}
  >
    {children}
  </StyledButton>
);

export default Button;
