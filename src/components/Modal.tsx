import React, { ReactNode, useState } from 'react';
import ReactModal from 'react-modal';
import Box from 'components/Box';
import { useTheme } from '@emotion/react';

// set up modal
ReactModal.setAppElement('#root');

export interface ModalProps {
  open?: boolean;
  children: ReactNode;
}

export const useModal = () => {
  const [open, setOpen] = useState<boolean>(false);
  const show = () => setOpen(true);
  const hide = () => setOpen(false);
  return {
    open,
    show,
    hide,
  };
};

const useStyles = () => {
  const theme = useTheme();
  return {
    overlay: {
      // @ts-ignore
      backgroundColor: theme.colors.overlay,
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      border: 'none',
      borderRadius: 0,
      padding: 0,
      background: 'transparent',
      width: '80%',
    },
  };
};

const Modal = ({ open = false, children }: ModalProps): JSX.Element => {
  const styles = useStyles();
  return (
    <ReactModal isOpen={open} style={styles}>
      <Box
        bg="card"
        p={4}
        borderRadius={8}
        margin="0 auto"
        width={['100%', 300, 300]}
      >
        {children}
      </Box>
    </ReactModal>
  );
};

export default Modal;
