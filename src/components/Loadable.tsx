import React, { ReactNode } from 'react';
import Box from 'components/Box';
import Loader from 'components/Loader';

interface LoadableProps {
  loading?: boolean;
  size?: number;
  children?: ReactNode;
}

const Loadable = ({
  size = 50,
  loading,
  children,
}: LoadableProps): JSX.Element => (
  <Box
    display={loading ? 'flex' : 'block'}
    alignItems="center"
    justifyContent="center"
  >
    {loading ? <Loader size={size} /> : children}
  </Box>
);

export default Loadable;
