import React from 'react';
import CurrencyInputField, {
  CurrencyInputProps,
} from 'react-currency-input-field';
import styled from '@emotion/styled';

interface InputProps extends Omit<CurrencyInputProps, 'onChange'> {
  onChange?: (value: string) => void;
}

const CurrencyInput = styled(CurrencyInputField)`
  appearance: none;
  border: 0;
  outline: none;
  font-size: 20px;
  font-weight: 600;
  text-align: right;
  width: 100%;
  padding: 2px;
`;

const Input = ({ onChange, ...rest }: InputProps): JSX.Element => {
  const handleChange = (value: string = '') => onChange && onChange(value);
  return (
    <CurrencyInput
      allowDecimals
      allowNegativeValue={false}
      decimalsLimit={2}
      {...rest}
      onValueChange={handleChange}
    />
  );
};

export default Input;
