import React, { useMemo } from 'react';
import { CheckmarkCircle } from '@emotion-icons/fluentui-system-regular';
import Box from 'components/Box';
import Text from 'components/Text';
import Button from 'components/Button';
import { useTheme } from '@emotion/react';
import { currency as displayCurrency } from 'utils/display';
import { Transaction, TransactionItem } from 'types';

interface TransactionDetailsProps {
  transaction: Transaction;
  onClose: () => void;
}

const TransactionDetails = ({
  transaction,
  onClose,
}: TransactionDetailsProps): JSX.Element => {
  // @ts-ignore
  const iconColor = useTheme().colors.brand;
  const [from, to] = useMemo(() => {
    return transaction.items.reduce((acct, item) => {
      if (item.amount > 0) return [acct[0], item];
      return [item, acct[1]];
    }, [] as Array<TransactionItem>);
  }, [transaction.items]);
  const amountFrom = displayCurrency(Math.abs(from.amount), from.currency);
  const amountTo = displayCurrency(to.amount, to.currency);
  return (
    <Box pt={4} pb={2}>
      <Box textAlign="center">
        <CheckmarkCircle size={80} color={iconColor} />
      </Box>
      <Box mt={4}>
        <Text fontSize={3} fontWeight="bold" textAlign="center">
          You exchanged
        </Text>
      </Box>
      <Box mt={2}>
        <Text fontSize={3} fontWeight="bold" textAlign="center" color="brand">
          {amountFrom} to {amountTo}
        </Text>
      </Box>
      <Box mt={6}>
        <Button variant="outline" onClick={onClose}>
          Cool
        </Button>
      </Box>
    </Box>
  );
};

export default TransactionDetails;
