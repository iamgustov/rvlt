import styled from '@emotion/styled';
import {
  typography,
  space,
  color,
  size,
  layout,
  flexbox,
  border,
  compose,
  position,
  system,
} from 'styled-system';

const cursorDecoration = system({
  cursor: {
    property: 'cursor',
  },
});

const Box = styled('div')(
  compose(
    cursorDecoration,
    position,
    typography,
    space,
    color,
    size,
    layout,
    flexbox,
    border,
  ),
);

export default Box;
