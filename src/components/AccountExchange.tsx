import React from 'react';
import { ArrowDown, ArrowUp } from '@emotion-icons/fluentui-system-regular';
import Box from 'components/Box';
import AccountBalance from 'components/AccountBalance';
import { useTheme } from '@emotion/react';
import { Account, ExchangeType, TransferType } from 'types';

interface AccountExchangeProps {
  type: ExchangeType;
  from: Account;
  amountFrom?: number;
  fromExceeds?: boolean;
  to: Account;
  amountTo?: number;
  toExceeds?: boolean;
  accounts: Array<Account>;
  onTypeToggle: () => void;
  onAccountChange: (type: TransferType, account: Account) => void;
  onAmountChange: (type: TransferType, amount: number) => void;
}

interface AccountExchangeSwitchProps {
  type: ExchangeType;
  onClick?: () => void;
}

const AccountExchangeSwitch = ({
  type,
  onClick,
}: AccountExchangeSwitchProps): JSX.Element => {
  // @ts-ignore
  const iconColor = useTheme().colors.brand;
  const Icon = type === 'sell' ? ArrowDown : ArrowUp;
  return (
    <Box
      width={24}
      height={24}
      cursor="pointer"
      display="inline-flex"
      alignItems="center"
      justifyContent="center"
      borderRadius="50%"
      borderWidth={4}
      borderColor="bg"
      borderStyle="solid"
      bg="card"
      onClick={onClick}
    >
      <Icon size={14} color={iconColor} />
    </Box>
  );
};

const AccountExchange = ({
  type,
  from,
  amountFrom,
  fromExceeds = false,
  to,
  amountTo,
  toExceeds = false,
  accounts,
  onAccountChange,
  onAmountChange,
  onTypeToggle,
}: AccountExchangeProps): JSX.Element => (
  <>
    <AccountBalance
      type={type === 'sell' ? 'sell' : 'buy'}
      account={from}
      accounts={accounts}
      amount={amountFrom}
      exceeds={fromExceeds}
      onAmountChange={onAmountChange.bind(null, 'from')}
      onAccountChange={onAccountChange.bind(null, 'from')}
    />
    <Box display="flex" justifyContent="center" my={-2}>
      <AccountExchangeSwitch type={type} onClick={onTypeToggle} />
    </Box>
    <AccountBalance
      type={type === 'sell' ? 'buy' : 'sell'}
      account={to}
      accounts={accounts}
      amount={amountTo}
      exceeds={toExceeds}
      onAmountChange={onAmountChange.bind(null, 'to')}
      onAccountChange={onAccountChange.bind(null, 'to')}
    />
  </>
);

export default AccountExchange;
