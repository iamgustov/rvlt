import { useEffect } from 'react';
import * as Api from 'api';
import { useStore } from 'store';

const REFETCH_INTERVAL = 10000; // 10 secs

const useRatesSync = () => {
  const store = useStore();

  useEffect(() => {
    let timeoutId: NodeJS.Timeout | undefined;
    const perform = async () => {
      const rates = await Api.fetchRates();
      store.setRates(rates);
      timeoutId = setTimeout(perform, REFETCH_INTERVAL);
    };
    perform();
    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

export default useRatesSync;
