import { Account, TransferType } from 'types';
import { useStore } from 'store';
import { useModal } from 'components/Modal';

const useExchange = () => {
  const exchangeModal = useModal();
  const {
    from,
    to,
    accounts,
    rate,
    isValid,
    amountFrom,
    amountTo,
    type,
    rates,
    lastTransaction,
    isFromExceeds,
    isToExceeds,
    exchange,
    changeFromAccount,
    changeToAccount,
    changeAmountFrom,
    changeAmountTo,
    toggleType,
  } = useStore();

  // change account
  const changeAccount = (type: TransferType, account: Account) => {
    if (type === 'from') {
      changeFromAccount(account);
    } else {
      changeToAccount(account);
    }
  };

  // handle amount change
  const changeAmount = (type: TransferType, amount: number) => {
    if (type === 'from') {
      changeAmountFrom(amount);
    } else {
      changeAmountTo(amount);
    }
  };

  // handle exchange
  const handleExchange = () => {
    exchange();
    exchangeModal.show();
  };

  return {
    type,
    amountFrom,
    amountTo,
    rate,
    from,
    to,
    accounts,
    toggleType,
    changeAccount,
    changeAmount,
    exchange: handleExchange,
    isValid,
    isFromExceeds,
    isToExceeds,
    isLoading: !rates,
    lastTransaction,
    modal: exchangeModal,
  };
};

export default useExchange;
