import React from 'react';
import { observer } from 'mobx-react-lite';
import Box from 'components/Box';
import Text from 'components/Text';
import ExchangeRate from 'components/ExchangeRate';
import AccountExchange from 'components/AccountExchange';
import Button from 'components/Button';
import Loadable from 'components/Loadable';
import Modal from 'components/Modal';
import TransactionDetails from 'components/TransactionDetails';
import useExchange from 'hooks/useExchange';
import { capitalize } from 'utils/display';

const ExchangeScreen = () => {
  const exchange = useExchange();
  const typeName = capitalize(exchange.type);
  return (
    <Loadable loading={exchange.isLoading}>
      <Text fontSize={5} textTransform="capitalize" fontWeight="600">
        {typeName} {exchange.from.currency}
      </Text>
      <Box mt={2}>
        <ExchangeRate
          rate={exchange.rate}
          from={exchange.from.currency}
          to={exchange.to.currency}
        />
      </Box>
      <Box mt={4} mb={6}>
        <AccountExchange
          type={exchange.type}
          from={exchange.from}
          amountFrom={exchange.amountFrom}
          fromExceeds={exchange.isFromExceeds}
          to={exchange.to}
          amountTo={exchange.amountTo}
          toExceeds={exchange.isToExceeds}
          accounts={exchange.accounts}
          onTypeToggle={exchange.toggleType}
          onAmountChange={exchange.changeAmount}
          onAccountChange={exchange.changeAccount}
        />
      </Box>
      <Button disabled={!exchange.isValid} onClick={exchange.exchange}>
        {typeName} {exchange.from.currency} to {exchange.to.currency}
      </Button>
      <Modal open={exchange.modal.open}>
        {exchange.lastTransaction && (
          <TransactionDetails
            transaction={exchange.lastTransaction}
            onClose={exchange.modal.hide}
          />
        )}
      </Modal>
    </Loadable>
  );
};

export default observer(ExchangeScreen);
