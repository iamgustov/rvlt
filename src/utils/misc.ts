export const uniqId = (prefix = '_') =>
  `${prefix}${Math.random().toString(36).substr(2, 9)}`;

export const round = (value: number) =>
  Math.round((value + Number.EPSILON) * 100) / 100;
