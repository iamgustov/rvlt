import { Currency, CurrencyRate, Transaction } from 'types';
import { round, uniqId } from 'utils/misc';

type RateOfParams = {
  from: Currency;
  to: Currency;
  rates: CurrencyRate;
};

type ExchangeParams = {
  from: Currency;
  to: Currency;
  rates: CurrencyRate;
  amount: number;
};

const DEFAULT_RATE = 1;
export const rateOf = ({ from, to, rates }: RateOfParams): number => {
  if (!rates) return DEFAULT_RATE;
  const baseRate = rates[from] ?? DEFAULT_RATE;
  const toRate = rates[to] ?? DEFAULT_RATE;
  return toRate / baseRate;
};

export const exchange = ({
  from,
  to,
  rates,
  amount,
}: ExchangeParams): Transaction => {
  const currentRate = rateOf({ from, to, rates });
  const exchangeAmount = round(currentRate * amount);
  return {
    id: uniqId((+new Date()).toString()),
    items: [
      { currency: from, amount: -amount },
      { currency: to, amount: exchangeAmount },
    ],
  };
};
