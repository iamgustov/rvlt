import { Currency } from 'types';

export const currency = (value: number, code: Currency): string => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: code,
  });
  return formatter.format(value);
};

export const capitalize = (value: string): string =>
  value.toString().replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());
