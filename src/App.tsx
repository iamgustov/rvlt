import React from 'react';
import Container from 'components/Container';
import Styles from 'components/Styles';
import ExchangeScreen from 'screens/ExchangeScreen';
import useRatesSync from 'hooks/useRatesSync';

const App = (): JSX.Element => {
  useRatesSync();
  return (
    <Container>
      <Styles />
      <ExchangeScreen />
    </Container>
  );
};

export default App;
